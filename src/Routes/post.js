const { Router } = require('express')
const router = Router()
const Post = require('../Models/Post');
const { ethers } = require('hardhat');
const { abi } = require('../../artifacts/contracts/CarRegistry.sol/CarRegistry.json');

const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL);
//const signer = new ethers.Wallet(req.session.user.metamask, provider)
const contractInstance = new ethers.Contract(process.env.CONTRACT_ADDRESS, abi, provider);


// router.post('/addCar', async (req, res) => {
//   const reviewedBy = req.session.user.email
//   const vin = req.body.vin
//   const name = req.body.name
//   const brand = req.body.brand
//   const motor = req.body.motor
//   const mileage = req.body.mileage
//   const owner = req.body.owner
//   const yop = req.body.yop

//   try {
//     const transaction = await contractInstance.addCar(vin, name, brand, motor, mileage, owner, yop, reviewedBy);
//     await transaction.wait();
//     res.redirect('/api/v1/post/postList')
//   } catch (error) {
//       console.error(error);
//       res.status(500).json({ message: "Failed to add car" });
//   }
// })


// router.get('/ispis', async(req, res) => {
//     var cars = await contractInstance.getAllCars();
//     console.log(cars[0].name);
// })

router.use((req, res, next) => {
        
    if(req.session.user){ 
        next();
    }else {
        res.redirect('/api/v1/auth/userlogin')
    }
  })


router.get('/postList', async(req, res) => {
    const user = req.session.user

    if (user.isAdmin) {
      try {
        var cars = await contractInstance.getAllCarsByReviewedBy(user.email)
        res.render('index', {cars, user, searchParameter:null})
      } catch (error){
        console.error(error);
        res.status(500).json({ message: "Failed to fetch cars" });
      }
    } else {
      try {
        var cars = await contractInstance.getAllCars();
        res.render('index', {cars, user, searchParameter:null})
      } catch (error){
        console.error(error);
        res.status(500).json({ message: "Failed to fetch cars" });
      }
    }

    // try {
    //     const posts = await Post.find().exec();
    //     res.render('index', {posts: posts, user, searchParameter:null})
    //   } catch (error) {
    //     console.error('Greška prilikom čitanja:', error);
        
    //   }

})

router.post('/postSearch', async(req, res) => {
  const user = req.session.user
  const searchParameter = req.body.search

  try {
    var cars = await contractInstance.getAllCarsByVin(searchParameter);
    res.render('index', { cars, user, searchParameter });
  } catch (error){
    console.error(error);
    res.status(500).json({ message: "Failed to fetch cars" });
  }

})

// router.post('/addPost', async (req, res) => {
//     const reviewedBy = req.session.user.email
//     const vin = req.body.vin
//     const name = req.body.name
//     const brand = req.body.brand
//     const motor = req.body.motor
//     const mileage = req.body.mileage
//     const owner = req.body.owner
//     const yop = req.body.yop
    
//     const newPost = await Post.create({ vin, name, brand, motor, mileage, owner, yop, reviewedBy })
//     res.redirect('/api/v1/post/postList')
// })


  module.exports = router